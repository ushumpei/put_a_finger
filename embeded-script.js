window.onload = function(){

  var progressBar = document.createElement("div");
  progressBar.className = "progress-bar";
  progressBar.setAttribute("style", "height: 2px;width: 100%;z-index: 100;position: fixed;top: 0;left: 0;");

  amount = document.createElement("div");
  amount.className = "amount";
  amount.setAttribute("style", "height: 100%; width: 0; background-color: blue;");

  progressBar.appendChild(amount);
  document.body.insertBefore(progressBar, document.body.firstChild);

  (function(){

    var documentBody = document.body;

    var progressBar = document.getElementsByClassName("amount");

    window.onscroll = function(){

      var documentHeight = documentBody.clientHeight;

      var scrollTop = documentBody.scrollTop;

      var baseWidth = documentHeight - screen.height;

      var parsent = scrollTop / baseWidth * 100;

      progressBar[0].style.width = parsent + "%";

    }
  })();
}
